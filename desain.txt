<?xml version="1.0" encoding="UTF-8" ?>

<Form version="1.3" maxVersion="1.9" type="org.netbeans.modules.form.forminfo.JFrameFormInfo">
  <NonVisualComponents>
    <Component class="javax.swing.JTextField" name="jTextField1">
      <Properties>
        <Property name="text" type="java.lang.String" value="jTextField1"/>
      </Properties>
    </Component>
  </NonVisualComponents>
  <Properties>
    <Property name="defaultCloseOperation" type="int" value="3"/>
  </Properties>
  <SyntheticProperties>
    <SyntheticProperty name="formSizePolicy" type="int" value="1"/>
    <SyntheticProperty name="generateCenter" type="boolean" value="false"/>
  </SyntheticProperties>
  <AuxValues>
    <AuxValue name="FormSettings_autoResourcing" type="java.lang.Integer" value="0"/>
    <AuxValue name="FormSettings_autoSetComponentName" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_generateFQN" type="java.lang.Boolean" value="true"/>
    <AuxValue name="FormSettings_generateMnemonicsCode" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_i18nAutoMode" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_layoutCodeTarget" type="java.lang.Integer" value="1"/>
    <AuxValue name="FormSettings_listenerGenerationStyle" type="java.lang.Integer" value="0"/>
    <AuxValue name="FormSettings_variablesLocal" type="java.lang.Boolean" value="false"/>
    <AuxValue name="FormSettings_variablesModifier" type="java.lang.Integer" value="2"/>
  </AuxValues>

  <Layout>
    <DimensionLayout dim="0">
      <Group type="103" groupAlignment="0" attributes="0">
          <Group type="102" alignment="0" attributes="0">
              <EmptySpace min="-2" pref="31" max="-2" attributes="0"/>
              <Group type="103" groupAlignment="0" max="-2" attributes="0">
                  <Component id="text1" min="-2" pref="341" max="-2" attributes="0"/>
                  <Group type="102" alignment="0" attributes="0">
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Component id="cmdclear" pref="74" max="32767" attributes="0"/>
                          <Component id="cmd7" alignment="1" max="32767" attributes="0"/>
                          <Component id="cmd4" alignment="1" max="32767" attributes="0"/>
                          <Component id="cmd1" alignment="1" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Component id="cmd8" pref="75" max="32767" attributes="0"/>
                          <Component id="cmd5" max="32767" attributes="0"/>
                          <Component id="cmd2" max="32767" attributes="0"/>
                          <Component id="cmd0" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" max="-2" attributes="0">
                          <Component id="cmd3" max="32767" attributes="0"/>
                          <Component id="cmd6" max="32767" attributes="0"/>
                          <Component id="cmd9" max="32767" attributes="0"/>
                          <Component id="cmdtambah" pref="70" max="32767" attributes="0"/>
                      </Group>
                      <EmptySpace max="-2" attributes="0"/>
                      <Group type="103" groupAlignment="0" attributes="0">
                          <Component id="cmdbagi" max="32767" attributes="0"/>
                          <Component id="cmdkali" max="32767" attributes="0"/>
                          <Component id="cmdkurang" alignment="0" max="32767" attributes="0"/>
                          <Component id="cmdsmdengan" alignment="0" max="32767" attributes="0"/>
                      </Group>
                  </Group>
              </Group>
              <EmptySpace min="0" pref="28" max="32767" attributes="0"/>
          </Group>
      </Group>
    </DimensionLayout>
    <DimensionLayout dim="1">
      <Group type="103" groupAlignment="0" attributes="0">
          <Group type="102" alignment="0" attributes="0">
              <EmptySpace max="-2" attributes="0"/>
              <Component id="text1" min="-2" pref="59" max="-2" attributes="0"/>
              <EmptySpace max="-2" attributes="0"/>
              <Group type="103" groupAlignment="3" attributes="0">
                  <Component id="cmd1" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd2" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd3" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmdbagi" alignment="3" min="-2" max="-2" attributes="0"/>
              </Group>
              <EmptySpace type="unrelated" max="-2" attributes="0"/>
              <Group type="103" groupAlignment="3" attributes="0">
                  <Component id="cmd4" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd5" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd6" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmdkali" alignment="3" min="-2" max="-2" attributes="0"/>
              </Group>
              <EmptySpace type="unrelated" max="-2" attributes="0"/>
              <Group type="103" groupAlignment="3" attributes="0">
                  <Component id="cmd7" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd8" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd9" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmdkurang" alignment="3" min="-2" max="-2" attributes="0"/>
              </Group>
              <EmptySpace type="unrelated" max="-2" attributes="0"/>
              <Group type="103" groupAlignment="3" attributes="0">
                  <Component id="cmdclear" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmd0" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmdtambah" alignment="3" min="-2" max="-2" attributes="0"/>
                  <Component id="cmdsmdengan" alignment="3" min="-2" max="-2" attributes="0"/>
              </Group>
              <EmptySpace pref="30" max="32767" attributes="0"/>
          </Group>
      </Group>
    </DimensionLayout>
  </Layout>
  <SubComponents>
    <Component class="javax.swing.JTextField" name="text1">
    </Component>
    <Component class="javax.swing.JButton" name="cmd1">
      <Properties>
        <Property name="text" type="java.lang.String" value="1"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd1ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd2">
      <Properties>
        <Property name="text" type="java.lang.String" value="2"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd2ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd3">
      <Properties>
        <Property name="text" type="java.lang.String" value="3"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd3ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd4">
      <Properties>
        <Property name="text" type="java.lang.String" value="4"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd4ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd5">
      <Properties>
        <Property name="text" type="java.lang.String" value="5"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd5ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd6">
      <Properties>
        <Property name="text" type="java.lang.String" value="6"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd6ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd7">
      <Properties>
        <Property name="text" type="java.lang.String" value="7"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd7ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd8">
      <Properties>
        <Property name="text" type="java.lang.String" value="8"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd8ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd9">
      <Properties>
        <Property name="text" type="java.lang.String" value="9"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd9ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmdbagi">
      <Properties>
        <Property name="text" type="java.lang.String" value="/"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmdbagiActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmdkali">
      <Properties>
        <Property name="text" type="java.lang.String" value="*"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmdkaliActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmdkurang">
      <Properties>
        <Property name="text" type="java.lang.String" value="-"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmdkurangActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmdclear">
      <Properties>
        <Property name="text" type="java.lang.String" value="C"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmdclearActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmd0">
      <Properties>
        <Property name="text" type="java.lang.String" value="0"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmd0ActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmdtambah">
      <Properties>
        <Property name="text" type="java.lang.String" value="+"/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmdtambahActionPerformed"/>
      </Events>
    </Component>
    <Component class="javax.swing.JButton" name="cmdsmdengan">
      <Properties>
        <Property name="text" type="java.lang.String" value="="/>
      </Properties>
      <Events>
        <EventHandler event="actionPerformed" listener="java.awt.event.ActionListener" parameters="java.awt.event.ActionEvent" handler="cmdsmdenganActionPerformed"/>
      </Events>
    </Component>
  </SubComponents>
</Form>
